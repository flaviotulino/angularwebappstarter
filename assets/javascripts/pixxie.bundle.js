var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var api;
(function (api) {
    /**
     * This class provides methods to fetch and send server data
     */
    var Api = (function () {
        function Api($http) {
            this.$http = $http;
            // Configure the endpoint basing on the running environment
            this.BASE_URL = window.location.hostname == "localhost" ? api.config.DEV_ENDPOINT : api.config.PRODUCTION_ENDPOINT;
        }
        /**
         * Makes an HTTP request using the get method providing
         * url interpolation with the BASE_URL
         * @param url String
         * @returns {any}
         */
        Api.prototype.get = function (url) {
            return this.$http.get(this.BASE_URL + url);
        };
        /**
         * Makes an HTTP request using the post method providing
         * url interpolation with the BASE_URL
         * @param url String
         * @param data Object
         * @returns {any}
         */
        Api.prototype.post = function (url, data) {
            return this.$http.post(this.BASE_URL + url, data);
        };
        /**
         * Makes an HTTP request using the put method providing
         * url interpolation with the BASE_URL
         * @param url String
         * @param data Object
         * @returns {any}
         */
        Api.prototype.put = function (url, data) {
            return this.$http.put(this.BASE_URL + url, data);
        };
        /**
         * Makes an HTTP request using the delete method providing
         * url interpolation with the BASE_URL
         * @param url String
         * @returns {any}
         */
        Api.prototype.delete = function (url) {
            return this.$http.delete(this.BASE_URL + url);
        };
        Api.$inject = ['$http'];
        return Api;
    }());
    api.Api = Api;
})(api || (api = {}));
var api;
(function (api) {
    var User = (function (_super) {
        __extends(User, _super);
        function User() {
            _super.apply(this, arguments);
        }
        User.prototype.test = function () {
            console.log(this.BASE_URL);
            return this.get('someurl');
        };
        return User;
    }(api.Api));
    api.User = User;
})(api || (api = {}));
var api;
(function (api) {
    api.config = {
        DEV_ENDPOINT: 'localhost/api/',
        PRODUCTION_ENDPOINT: 'www.api.com/'
    };
})(api || (api = {}));
/**
 * This directive replaces the navbar custom tag with the relative view
 */
var directives;
(function (directives) {
    navbar.$inject = [];
    function navbar() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/_navbar.html',
        };
    }
    directives.navbar = navbar;
})(directives || (directives = {}));
/**
 * Replaces the <sidebar> tag with the relative view
 */
var directives;
(function (directives) {
    sidebar.$inject = ['$timeout'];
    function sidebar($timeout) {
        return {
            restrict: 'E',
            templateUrl: 'views/_sidebar.html',
            replace: true,
            link: function (scope, el, attrs) {
                $timeout(function () {
                    $('.sidebar-toggle,.sidebar-backdrop').on('click touchstart', function (e) {
                        $('.sidebar,.sidebar-backdrop').toggleClass('is-visible');
                        e.preventDefault();
                    });
                });
            }
        };
    }
    directives.sidebar = sidebar;
})(directives || (directives = {}));
/**
 * Defines the <page> tag which created a section using the state name as reference
 */
var directives;
(function (directives) {
    page.$inject = [];
    function page() {
        return {
            restrict: "E",
            transclude: true,
            replace: true,
            template: "<section class='page {{page}}-view' ng-transclude></section>"
        };
    }
    directives.page = page;
})(directives || (directives = {}));
///<reference path="../../api/User.ts"/>
///<reference path="../../assets/javascripts/directives/directives.d.ts" />
var application = angular.module('Pixxie', ['ui.router', 'ngSanitize']);
var bootstrap;
(function (bootstrap) {
    function config($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state({
            name: 'login',
            url: '/login',
            controller: controllers.LoginController
        })
            .state({
            name: 'about',
            url: '/about',
            template: 'sono un about'
        })
            .state({
            name: "dashboard",
            abstract: true,
            url: '/',
            templateUrl: "views/dashboard.html"
        })
            .state({
            name: "dashboard.home",
            url: "",
            controller: function ($rootScope, $http) {
                $rootScope.userIsLogged = true;
                console.log(new api.User($http).test());
            }
        });
        $urlRouterProvider.otherwise('/login');
    }
    bootstrap.config = config;
    config.$inject = ["$stateProvider", "$urlRouterProvider"];
    function run($rootScope, $http) {
        // by convention use STATE_NAME.html template defined in the views/ folder
        // or, if defined, the string provided
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
            toState.templateUrl = toState.templateUrl || "views/" + toState.name + ".html";
        });
        // Track which state is visited and update the page attribute attached to body
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {
            $rootScope.page = toState.name;
        });
        // Load locales
        $http.get('locales/' + navigator.language + '.bundle.json').then(function (res) {
            // get the I18n module to interpolate strings
            $rootScope.I18n = I18n;
            // Expose the loaded locales
            $rootScope.l = res.data;
        });
    }
    bootstrap.run = run;
    run.$inject = ["$rootScope", "$http"];
})(bootstrap || (bootstrap = {}));
application.config(bootstrap.config);
// Directives auto-load
Object.keys(directives).map(function (directiveName) {
    application.directive(directiveName, directives[directiveName]);
});
// Add a specific factory; API one in this case
application.factory('Api', api.Api);
application.run(bootstrap.run);
/**
 * This is the base controller class.
 *
 * Auto-injects the angular $scope and creates a reference to it,
 * using the state name by convention
 *
 */
var BaseController = (function () {
    function BaseController($scope, $rootScope) {
        // let scopeName = "scope";
        // let constructor:any = this.constructor;
        //
        // if (constructor.name.match(/controller/i)) {
        //     scopeName = constructor.name.replace(/controller/i, '').toLowerCase();
        // }
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        var scopeName = $rootScope.page;
        this.$scope[scopeName] = this;
    }
    BaseController.$inject = ["$scope", "$rootScope"];
    return BaseController;
}());
var controllers;
(function (controllers) {
    var LoginController = (function (_super) {
        __extends(LoginController, _super);
        function LoginController($scope, $rootScope, $state) {
            _super.call(this, $scope, $rootScope);
            this.$scope = $scope;
            this.$rootScope = $rootScope;
            this.$state = $state;
            this.model = {
                email: "test@test.com",
                password: "11",
                rememberMe: false
            };
        }
        LoginController.prototype.submit = function () {
            this.$rootScope.userIsLogged = true;
            this.$state.go('dashboard.home');
        };
        LoginController.$inject = ['$scope', '$rootScope', "$state"];
        return LoginController;
    }(BaseController));
    controllers.LoginController = LoginController;
})(controllers || (controllers = {}));
/**
 * Use this class to provide interpolation when needed
 */
var I18n = (function () {
    function I18n() {
    }
    I18n.t = function (string, args) {
        if (angular.isDefined(args)) {
            var keys = Object.keys(args);
            // For each argument passed as object
            keys.map(function (key) {
                // replace %{SOME_KEY_HERE} with its value
                var expr = new RegExp("\%\{" + key + "\}");
                string = string.replace(expr, args[key]);
            });
        }
        return string;
    };
    return I18n;
}());
//# sourceMappingURL=pixxie.bundle.js.map