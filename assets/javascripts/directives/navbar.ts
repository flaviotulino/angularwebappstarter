/**
 * This directive replaces the navbar custom tag with the relative view
 */
namespace directives {
    navbar.$inject = [];
    export function navbar() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/_navbar.html',
        }
    }
}