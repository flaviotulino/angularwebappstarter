///<reference path="../../api/User.ts"/>
///<reference path="../../assets/javascripts/directives/directives.d.ts" />

let application = angular.module('Pixxie', ['ui.router', 'ngSanitize']);

namespace bootstrap {
    export function config($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state({
                name: 'login',
                url: '/login',
                controller: controllers.LoginController
            })
            .state({
                name: 'about',
                url: '/about',
                template: 'sono un about'
            })

            .state({
                name: "dashboard",
                abstract: true,
                url: '/',
                templateUrl: "views/dashboard.html"
            })

            .state({
                name: "dashboard.home",
                url: "",
                controller: ($rootScope, $http) => {
                    $rootScope.userIsLogged = true;
                    console.log(new api.User($http).test());
                }
            });

        $urlRouterProvider.otherwise('/login');
    }

    config.$inject = ["$stateProvider", "$urlRouterProvider"];

    export function run($rootScope, $http) {

        // by convention use STATE_NAME.html template defined in the views/ folder
        // or, if defined, the string provided
        $rootScope.$on('$stateChangeStart',
            (event, toState, toParams, fromState, fromParams, options) => {
                toState.templateUrl = toState.templateUrl || "views/" + toState.name + ".html";
            });


        // Track which state is visited and update the page attribute attached to body
        $rootScope.$on('$stateChangeSuccess',
            (event, toState, toParams, fromState, fromParams, options) => {
                $rootScope.page = toState.name;
            });


        // Load locales
        $http.get('locales/' + navigator.language + '.bundle.json').then(res=> {
            // get the I18n module to interpolate strings
            $rootScope.I18n = I18n;

            // Expose the loaded locales
            $rootScope.l = res.data;
        });
    }

    run.$inject = ["$rootScope", "$http"];
}

application.config(bootstrap.config);

// Directives auto-load
Object.keys(directives).map((directiveName) => {
   application.directive(directiveName,directives[directiveName]);
});

// Add a specific factory; API one in this case
application.factory('Api',api.Api);

application.run(bootstrap.run);

