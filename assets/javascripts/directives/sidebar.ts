/**
 * Replaces the <sidebar> tag with the relative view
 */
namespace directives {
    sidebar.$inject = ['$timeout'];
    export function sidebar($timeout) {
        return {
            restrict: 'E',
            templateUrl: 'views/_sidebar.html',
            replace:true,
            link: (scope:any, el, attrs) => {
                $timeout(() => {
                    $('.sidebar-toggle,.sidebar-backdrop').on('click touchstart', function (e) {
                        $('.sidebar,.sidebar-backdrop').toggleClass('is-visible');
                        e.preventDefault();
                    });
                })
            }
        }
    }
}