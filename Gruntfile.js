// Gruntfile with the configuration of grunt-express and grunt-open. No livereload yet!
module.exports = function (grunt) {

    // Load Grunt tasks declared in the package.json file
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    grunt.loadNpmTasks('grunt-merge-json');


    // Configure Grunt
    grunt.initConfig({

        // grunt-express will serve the files from the folders listed in `bases`
        // on specified `port` and `hostname`
        express: {
            all: {
                options: {
                    port: 9000,
                    hostname: "0.0.0.0",
                    bases: [__dirname], // Replace with the directory you want the files served from
                                        // Make sure you don't use `.` or `..` in the path as Express
                                        // is likely to return 403 Forbidden responses if you do
                                        // http://stackoverflow.com/questions/14594121/express-res-sendfile-throwing-forbidden-error
                    livereload: true
                }
            }
        },

        "merge-json": {
            it: {
                src: ["!locales/**/it.bundle.json", "locales/**/it*.json"],
                dest: "locales/it.bundle.json"
            },
            en: {
                src: ["!locales/**/en.bundle.json", "locales/**/en*.json"],
                dest: "locales/en.bundle.json"
            }
        },

        // grunt-watch will monitor the projects files
        watch: {
            all: {
                // Replace with whatever file you want to trigger the update from
                // Either as a String for a single entry
                // or an Array of String for multiple entries
                // You can use globing patterns like `css/**/*.css`
                // See https://github.com/gruntjs/grunt-contrib-watch#files
                files: 'index.html',
                tasks: ['merge-json'],
                options: {
                    livereload: true
                }
            }
        },

        // grunt-open will open your browser at the project's URL
        open: {
            all: {
                // Gets the port from the connect configuration
                path: 'http://localhost:<%= express.all.options.port%>'
            }
        },


        // Tasks for dist command
        clean: ['dist'],
        copy: {
            all: {
                files: [
                    {expand: true, src: ['index.html'], dest: 'dist/'},
                    // {expand:true, src: ['']}
                ]
            }
        },
        replace: {
            all: {
                options: {
                    patterns: [
                        {
                            match: 'script',
                            replacement: 'scra'
                        }
                    ]
                },
                files: [
                    {expand: true,src:['dist/index.html'],dest: 'dist/'}
                ]
            }
        }


    });

    // Creates the `server` task
    grunt.registerTask('server', [
        'merge-json',
        'express',
        'open',
        'watch'
    ]);

    grunt.registerTask('dist', [
        'clean',
        'copy',
        'replace'
    ]);

    grunt.registerTask('export-locales', [
        'merge-json'
    ])
};