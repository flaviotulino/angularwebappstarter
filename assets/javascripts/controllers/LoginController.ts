namespace controllers {
    interface LoginModel {
        email:string;
        password:string;
        rememberMe:boolean;
    }
    export class LoginController extends BaseController {

        private model:LoginModel;

        static $inject = ['$scope', '$rootScope', "$state"];

        constructor(protected $scope, protected $rootScope, protected $state) {
            super($scope, $rootScope);

            this.model = {
                email: "test@test.com",
                password: "11",
                rememberMe: false
            }
        }


        public submit() {
            this.$rootScope.userIsLogged = true;
            this.$state.go('dashboard.home');
        }
    }
}

