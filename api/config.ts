module api {
    export var config = {
        DEV_ENDPOINT: 'localhost/api/',
        PRODUCTION_ENDPOINT: 'www.api.com/'
    }
}